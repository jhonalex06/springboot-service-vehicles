package com.test.springbootapp.vehicles.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.test.springbootapp.vehicles.models.entity.vehicle;
import com.test.springbootapp.vehicles.models.service.IVehicleService;

@RestController
public class VehicleController {

	@Autowired
	private IVehicleService vehicleService;
	
	@GetMapping("/listar")
	public List<vehicle> listar(){
		return vehicleService.findAll();
	}
	
	@GetMapping("/ver/{id}")
	public vehicle detalle(@PathVariable Long id){
		return vehicleService.findById(id);
	}
	
	@GetMapping("/guardar/{matricula}/{latitud}/{longitud}")
	public vehicle salvar(@PathVariable String matricula, @PathVariable Double latitud, @PathVariable Double longitud){
		return vehicleService.save(matricula, latitud, longitud);
	}
	
}
