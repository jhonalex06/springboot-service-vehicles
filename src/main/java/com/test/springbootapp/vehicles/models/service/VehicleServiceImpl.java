package com.test.springbootapp.vehicles.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.springbootapp.vehicles.models.entity.vehicle;
import com.test.springbootapp.vehicles.models.repository.IVehicleRepository;

@Service
public class VehicleServiceImpl implements IVehicleService{

	@Autowired
	private IVehicleRepository vehiclerepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<vehicle> findAll() {
		return (List<vehicle>) vehiclerepository.findAll();
	}

	@Override
	public vehicle findById(Long id) {
		return vehiclerepository.findById(id).orElse(null);
	}

	@Override
	public vehicle save(String matricula, Double latitud, Double longitud) {
		vehicle vehiculo = new vehicle(matricula, latitud, longitud);
		return vehiclerepository.save(vehiculo);
	}

}
