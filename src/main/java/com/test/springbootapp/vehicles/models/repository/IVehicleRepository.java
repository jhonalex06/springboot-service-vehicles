package com.test.springbootapp.vehicles.models.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.test.springbootapp.vehicles.models.entity.vehicle;

@Repository
public interface IVehicleRepository extends CrudRepository<vehicle, Long>{

}
