package com.test.springbootapp.vehicles.models.service;

import java.util.List;
import com.test.springbootapp.vehicles.models.entity.vehicle;

public interface IVehicleService {

	public List<vehicle> findAll();
	public vehicle findById(Long id);
	public vehicle save(String matricula, Double latitud, Double longitud);
}
