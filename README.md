# springboot-service-vehicles

This application has the objective to save information about vehicles (Car_license_plate, latitude, longitude).

To save a registrer, you have to use the next command:

    Structure: https://service-vehicles.herokuapp.com/guardar/{Car_license_plate}/{latitude}/{longitude}
    Example: https://service-vehicles.herokuapp.com/guardar/BYQ242/4.6097100/-74.0817500
    
To see all data on the database, you have to use the next command:

    https://service-vehicles.herokuapp.com/listar
    
To see one register by ID, you have to use the next command:

    Structure: https://service-vehicles.herokuapp.com/ver/{id}
    Example: https://service-vehicles.herokuapp.com/ver/25
    
On the folder, Frontend is possible to deploy the applicatión to see the coordinates on the map, like this

![final](/uploads/fb12bf88dd1e8311093282e5b3b6b4b0/final.png)